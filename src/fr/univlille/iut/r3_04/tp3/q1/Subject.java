package fr.univlille.iut.r3_04.tp3.q1;


public abstract class Subject {

	protected void notifyObervers() {
	}

	protected void notifyObservers(Object data) {
	}

	public void attach(Observer observer) {
	}

	public void detach(Observer observer) {
	}
}
