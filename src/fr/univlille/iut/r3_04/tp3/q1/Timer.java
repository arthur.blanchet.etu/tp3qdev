package fr.univlille.iut.r3_04.tp3.q1;


public class Timer {

	public void start() {
	}

	public void stopRunning() {
	}

	public void attach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}

	public void detach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}

}
