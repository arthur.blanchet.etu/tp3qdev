package fr.univlille.iut.r3_04.tp3.q2;

public class ConnectableProperty extends ObservableProperty {

	public void connectTo(ConnectableProperty p2) {
	}

	public void biconnectTo(ConnectableProperty p2) {
	}

	public void unconnectFrom(ConnectableProperty p2) {
	}

}
