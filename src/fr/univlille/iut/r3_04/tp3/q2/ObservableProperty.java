package fr.univlille.iut.r3_04.tp3.q2;

import fr.univlille.iut.r3_04.tp3.q1.Observer;

public class ObservableProperty {

	public void setValue(Object i) {
	}

	public Object getValue() {
		return null;
	}

	public void attach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}

	public void detach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
	}

}
